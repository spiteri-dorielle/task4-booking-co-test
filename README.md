Booking&Co Test - Task 4

- App created using React

- Setup instructions
	- In the termal type 'npm install' to download dependencies
	- Then type 'npm start' to run the application. 
	- To stop press 'ctrl+C'.
	
- The application answers Task 4 of the test sheet 
- Description:
	- The application takes a number of miles as input. On click of the 'Calculate Stops' button
		a list of vessels will appear. Each vessel card holds the manufacurer and model of the vessel 
		as well as the time it takes for that particular vessel to cover the number of miles inputted by the user.
		The card also contains the number of days and stops. Please note that it might take a while for the list to appear. Ideally there would be a loader.
		
