import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { getVessels } from './redux/actions/vesselsActions';
import Vessels from './components/vessels';

import './styling/index.scss';
import 'bootstrap/scss/bootstrap.scss';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      vessels: [],
      inputValue: null,      
      showList: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);  }
  
  componentDidMount() { 
    this.dataLoadHandler();
  }

  async dataLoadHandler() {
    const {dispatch} = this.props;
    await dispatch(getVessels(this.state.inputValue));
    this.setState({ vessels: this.props.vessels });
  }

  handleChange(event){
    this.setState({
      inputValue: event.target.value
    });
  }

  handleSubmit(event) {
    this.dataLoadHandler();
    this.setState({ showList: true });
    event.preventDefault();
  }

  render(){
    return (
      <Fragment>
        <Container className="mt-5">

          <Form onSubmit={this.handleSubmit}>
            <Row>
              <Col md={8} sm={12}>
                <Form.Group controlId="milesInput">
                  <Form.Control size="lg" type="number" placeholder="Number of Miles" value={this.state.inputValue} onChange={this.handleChange}/>
                </Form.Group>
              </Col>
              <Col md={4} sm={12}>
                <Button variant="secondary" className="w-100" size="lg" type="submit"> Calculate Stops </Button>
              </Col>
            </Row>
          </Form>

          <Row className="mt-3">
            { 
              this.state.showList ?  
                this.state.vessels.vessels.map(vessel => {
                  return <Vessels manufacturer={vessel.manufacturer} model={vessel.model} time={vessel.time} stops={vessel.stops} />          
                }) : 
                <Col sm={12}>
                  <Alert variant="secondary"> Please insert miles to see the number of stops per vessel! </Alert>
                </Col>
            }      
          </Row>
        </Container>        
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({  
  vessels: state.vessels
});

export default connect(mapStateToProps)(App);