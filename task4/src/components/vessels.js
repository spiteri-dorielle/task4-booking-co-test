import React from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';

const Vessels = (props) => {
     return ( 
        <Col md="3" sm="12" className="mb-3">
            <Card border="secondary" className="vessel-card">
                <Card.Header className="vessel-details">{props.manufacturer}, {props.model}</Card.Header>
                <Card.Body class="computed-details">
                    <Card.Text>
                        <div class="time"> Time (Hrs):  {props.time} </div>
                        <div class="days"> Days:  {Math.round(props.time/24)} </div>
                        <div class="stops"> Stops: { props.stops} </div>
                    </Card.Text>
                </Card.Body>
            </Card>
         </Col>
    )
}

export default Vessels;