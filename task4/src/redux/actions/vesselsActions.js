import * as actions from './actions';
import axios from 'axios';

const api = axios.get('https://addajet.blob.core.windows.net/scripts/response.json');

export const getVessels = (miles) => {
    return async dispatch => {
        const response = await api;

        let vesselSpeed = [];
        let vesselsList = response.data;

        if(vesselsList != null){
            const list = vesselsList.map(v => {
                if(v.speed != "N/A"){
                    let time = Number((miles/v.speed).toFixed(2));
                    let consumableHrs = parseInt(v.consumables, 10) * 24;

                    let obj = { 
                        manufacturer: v.manufacturer, 
                        model: v.model, 
                        speed: v.speed, 
                        time: time,
                        consumableHrs: consumableHrs,
                        stops: consumableHrs < time ? Math.round(time/consumableHrs) : 0
                    };
            
                    vesselSpeed.push(obj);
                }
            })
        }

        dispatch({
            type: actions.GET_VESSELS,
            payload: vesselSpeed
        })
    }
};
