import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import vessels from './vesselsReducers';

const reducers = combineReducers({
    vessels: vessels
});

export default function initializeStore(state = {}) {
    return createStore(
        reducers,
        state,
        composeWithDevTools(applyMiddleware(thunkMiddleware))
    )
};