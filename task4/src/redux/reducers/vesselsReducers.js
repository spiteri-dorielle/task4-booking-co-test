import * as actions from '../actions/actions';

export default function vesselsReducer(state = {}, action){
    switch(action.type) {
        case actions.GET_VESSELS:
            return Object.assign({}, state, {
                vessels: action.payload
            })
        default:
            return state;
    }
}